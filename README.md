# What is MonteCoffee?
MonteCoffee is an application for performing kinetic Monte Carlo 
(kMC) simulations. The program is written as a set of Python modules, which
are meant to be used as a programmable application. In this sense, the user
modifies and develops on the code to customize as needed.

# Description and how to cite?
If you find MonteCoffee useful, please make a citation to:

[M. Jørgensen and H. Grönbeck, *J. Chem. Phys.* **(2018)**, *149*, 114101](https://doi.org/10.1063/1.5046635)


# Scientific articles using MonteCoffee
The following papers have used MonteCoffee:

[M. Jørgensen and H. Grönbeck, *J. Chem. Phys.* **(2018)**, *149*, 114101](https://doi.org/10.1063/1.5046635)

[M. Jørgensen and H. Grönbeck, *ACS Catal.* **(2017)**, *7*, 5054](https://doi.org/10.1021/acscatal.7b01194)

[M. Jørgensen and H. Grönbeck, *Angew. Chem. Int. Ed.* **(2018)**, *57*, 5086](https://doi.org/10.1002/anie.201802113)

[T. N. Pingel, M. Jørgensen, A. B. Yankovich, H. Grönbeck, and E. Olsson, *Nat. Commun.* **(2018)**, *9*, 2722](https://doi.org/10.1038/s41467-018-05055-1)

[M. Jørgensen and H. Grönbeck, *Top. Catal.* **(2019)**, *62*, 660](https://doi.org/10.1007/s11244-019-01145-6)

[M. Jørgensen and H. Grönbeck, *ACS Catal.* **(2019)**, *9*, 8872](https://doi.org/10.1021/acscatal.9b02228)

[N. Bosio and H. Grönbeck, *J. Phys. Chem. C* **(2020)**, *124*, 11952](https://doi.org/10.1021/acs.jpcc.0c02706)

[E.M. Dietze, L. Chen, and H. Grönbeck, *J. Chem. Phys.* **(2022)**, *156*, 064701](https://doi.org/10.1063/5.0078918)

[N. Bosio, M. Di, M. Skoglundh, P.A. Carlsson, H. Grönbeck, *J. Phys. Chem. C* **(2022)**, *126*, 16164](https://doi.org/10.1021/acs.jpcc.2c04833)

[R. Svensson and H. Grönbeck, *J. Am. Chem. Soc.* **(2023)**, *145*, 11579](https://doi.org/10.1021/jacs.3c00656)

# Running a MonteCoffee simulation
The *quick_example.py* in the documentation describes a minimal working example on how to run a simulation.

for a quick-start example of CO oxidation over a nanoparticle, you can run *python test.py* 

## Documentation
Is hosted on [montecoffee.readthedocs.io](https://montecoffee.readthedocs.io/)
and is built automatically after each push.

To build documentation manually, see *documentation/HOWTO_BUILD_DOCUMENTATION.txt*

## Developers

- Mikkel Jørgensen, Chalmers University of Technology, Sweden.

- Noemi Bosio, Chalmers University of Technology, Sweden.

- Elisabeth M. Dietze, Chalmers University of Technology, Sweden.

- Rasmus Svensson, Chalmers University of Technology, Sweden.

## Troubles / Ideas / Questions?

Please post an issue to the git, and we will try to reply ASAP.

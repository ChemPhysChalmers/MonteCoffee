.. _literature:
.. index:: Literature

Scientific articles using MonteCoffee
*************************

    - `M. Jørgensen and H. Grönbeck`, `Scaling Relations and Kinetic Monte Carlo Simulations to Bridge the Materials-Gap in Heterogeneous Catalysis <https://pubs.acs.org/doi/10.1021/acscatal.7b01194>`_, `ACS Catal.` **(2017)**, 7, 5054.
    - `M. Jørgensen and H. Grönbeck`, `The site-assembly determines catalytic activity of nanoparticles <https://doi.org/10.1002/anie.201802113>`_, `Angew. Chem. Int. Ed.` **(2018)**, 57, 5086.
    - `T. N. Pingel, M. Jørgensen, A. B. Yankovich, H. Grönbeck, and E. Olsson`, `Influence of Atomic Site-Specific Strain on Catalytic Activity of Supported Nanoparticles <https://doi.org/10.1038/s41467-018-05055-1>`_,`Nat. Commun.` **(2018)**, 9, 2722.
    - `M. Jørgensen and H. Grönbeck`, `MonteCoffee: A Programmable Kinetic Monte Carlo Framework <https://doi.org/10.1063/1.5046635>`_, `J. Chem. Phys.` **(2018)**, 149, 114101.
    - `M. Jørgensen and H. Grönbeck`, `Strain affects CO oxidation on metallic nanoparticles non-linearly <https://doi.org/10.1007/s11244-019-01145-6>`_, `Top. Catal.` **(2019)**, 62, 660.
    - `M. Jørgensen and H. Grönbeck`,  `Perspectives on Computational Catalysis for Metal Nanoparticles <https://doi.org/10.1021/acscatal.9b02228>`_, `ACS Catal.` **(2019)**, 9, 8872.
    - `M. Jørgensen and H. Grönbeck`, `Selective acetylene hydrogenation over single-atom alloy nanoparticles by kinetic Monte Carlo <https://doi.org/10.1021/jacs.9b02132>`_, `J. Am. Chem.Soc.` **(2019)**, 141, 8541.
    - `N. Bosio and H. Grönbeck`, `Sensitivity of Monte Carlo Simulations to Linear Scaling Relations <https://doi.org/10.1021/acs.jpcc.0c02706>`_, `J. Phys. Chem. C` **(2020)**, 124, 11952.
    - `E.M. Dietze, L. Chen, and H. Grönbeck`, `Surface steps dominate the water formation on Pd(111) surfaces <https://doi.org/10.1063/5.0078918>`_, `J. Chem. Phys.` **(2022)**, 156, 064701
    - `N. Bosio, M. Di, M. Skoglundh, P.A. Carlsson, H. Grönbeck`, `Interface reactions dominate low-temperature CO oxidation activity over Pt/CeO2 <https://doi.org/10.1021/acs.jpcc.2c04833>`_, `J. Phys. Chem. C` **(2022)**, 126, 16164.
    - `R. Svensson and H. Grönbeck`, `Site-Communication in Direct Formation of H2O2 over Single Atom Pd@Au Nanoparticles <https://doi.org/10.1021/jacs.3c00656>`_, `J. Am. Chem. Soc.` **(2023)**, 145, 11579.



Additional reading
*************************

The following resources can be very helpful in understanding 
kinetic Monte Carlo simulations:

    - `An Introduction to Kinetic Monte Carlo Simulations of Surface Reactions <https://www.springer.com/gp/book/9783642294877>`_ by A. P. J. Jansen.



Acceleration of kinetic Monte Carlo simulations is a topic of its own, which
is discussed in the book by A. P. J. Jansen, and the articles below:

    - `E. C. Dybeck et al.`, `Generalized Temporal Acceleration Scheme for Kinetic Monte Carlo Simulations of Surface Catalytic Processes by Scaling the Rates of Fast Reactions <https://doi.org/10.1021/acs.jctc.6b00859>`_, `J. Chem. Theory Comput.` **(2017)**, 13, 1525.
    
    
The PhD thesis of Mikkel Jørgensen discusses kMC simulations on nanoparticles, and some considerations about how :program:`MonteCoffee` was designed:

    - `Kinetics of Nanoparticle Catalysis from First Principles <https://research.chalmers.se/en/publication/508826>`_ by M. Jørgensen.
